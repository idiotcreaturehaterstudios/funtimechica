import guilded
from guilded.ext import commands
import random
import requests
import os
from dotenv import load_dotenv

load_dotenv()

client = guilded.Client()
client = commands.Bot(user_id='YOURBOTIDHERE', command_prefix=os.getenv("PREFIX"))
client.remove_command("help")

@client.event
async def on_ready():
    print("Funtime Chica Guilded Bot is Online and Ready")

@client.command()
async def help(ctx):
    embed = guilded.Embed(title="Funtime Chica Bot", description="help - This Message\nping - Checks my latency\npoll - Setup a poll in your server\nsay - Make me say things\nesay - Make me say things in a embed", color=(16711839))
    await ctx.send(embed=embed)

@client.command()
async def ping(ctx):
    await ctx.send(f'My Latency is: {round(client.latency * 1000)}ms')

@client.command()
async def poll(ctx, question, option1: str, option2: str):
    poll_embed = guilded.Embed(title=question, description=f"1️⃣ {option1}\n2️⃣ {option2}", color=(167935))
    new_msg = await ctx.send(embed=poll_embed)
    await new_msg.add_reaction('90002199')
    await new_msg.add_reaction('90002200')

@client.command()
async def say(ctx, *, question: commands.clean_content):
    await ctx.send(f'{question}')
    await ctx.message.delete()

@client.command()
async def esay(ctx, *, question: commands.clean_content):
    embed = guilded.Embed(description=f"{question}", color=(16711839))
    await ctx.send(embed=embed)
    await ctx.message.delete()





TOKEN = os.getenv("GUILDEDBOTTOKEN")
client.run(TOKEN)
