@echo off
title FUNTIME CHICA SELF-HOSTING SETUP

:menu
echo FUNTIME CHICA PYTHON PACKAGE INSTALLER
echo.
echo This setup bat script will install the following python packages in order to host your own Funtime Chica bot.
echo - discord.py
echo - guilded.py
echo - python-dotenv
echo - Voltage
echo - requests
echo.
echo Depending on your computer/server and your internet, installation times may vary but may require up to 5 minutes to install all packages.
echo.
echo Once you continue, you agree that we are not responsible for anything that happens during the installation process.
echo.
pause
goto install

:install
py -m pip install discord.py
py -m pip install discord
py -m pip install guilded.py
py -m pip install guilded
py -m pip install python-dotenv
py -m pip install voltage @ git+https://github.com/EnokiUN/voltage
py -m pip install requests
pause