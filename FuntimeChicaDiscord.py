import discord
from discord.ext import commands
import random
import datetime
import asyncio
import requests
import json
import sys
import aiohttp
import os
from dotenv import load_dotenv

load_dotenv()

intents = discord.Intents.default()
intents.message_content = True
client = commands.Bot(command_prefix=os.getenv("PREFIX"), intents=intents)
client.remove_command("help")

@client.event
async def on_ready():
    print("Funtime Chica Discord Bot is Online and Ready")

@client.command()
async def help(ctx):
    embed = discord.Embed(title="Funtime Chica", description="help - This Message\nping - Checks my latency\nbotinfo - Get information about me.\ngiveaway - Setup a giveaway in your server\npoll - Setup a 2 answer poll in your server\nthreepoll - Setup a 3 answer poll in your server\nsay - Make me say things\nserverinfo - Get information about your server\nuserinfo - Get information about a user", color=(16745984))
    embed.add_field(name="Other Commands", value='numberguessing - Play the Number Guessing game with the bot\nrps - Play Rock Paper Scissors with the bot\nwatchstatus - Set a watching status\nlistenstatus - Set a Listening status\nplaystatus - Set a playing status', inline=False)
    await ctx.send(embed=embed)

@client.command()
async def ping(ctx):
    await ctx.send(f'My Latency is: {round(client.latency * 1000)}ms')

@client.command()
async def botinfo(ctx):
    embed = discord.Embed(title="Funtime Chica Information", description=f"Made by: idiotcreaturehater (Developers ID: 757827467657478245)\nServers Im in: {len(client.guilds)}\nCurrent Latency: {round(client.latency * 1000)}ms", color=(16745984))
    embed.add_field(name="LINKS", value="Invite Me: https://discord.com/api/oauth2/authorize?client_id=1055570977855127572&permissions=0&scope=bot+applications.commands\nSupport Server: https://discord.gg/nVs4rCNwyc\nPrivacy Policy: https://funtimechica.netlify.app/privacy\Source Code: https://gitlab.com/idiotcreaturehaterstudios/funtimechica", inline=False)
    await ctx.send(embed=embed)

@client.command()
@commands.has_permissions(manage_guild=True)
async def giveaway(ctx, time_unit: str, winners: int, *, prize: str):
    embed = discord.Embed(title="Giveaway!", description=f"{prize}", color=discord.Color.random())
    embed.add_field(name="Hosted by:", value=ctx.author.mention)
    embed.add_field(name="React with 🎉 to enter!", value=f"Ends {time_unit} from now")
    msg = await ctx.send(embed=embed)
    await msg.add_reaction("🎉")
    duration_seconds = convert(time_unit)
    await asyncio.sleep(duration_seconds)
    new_msg = await ctx.channel.fetch_message(msg.id)
    
    users = []
    async for user in new_msg.reactions[0].users():
        if user != client.user:
            users.append(user)
            
    if winners < 1:
        winners = 1 
    if len(users) <= winners:
        selected_winners = users
    else:
        selected_winners = random.sample(users, winners)

    winners_mention = ", ".join(user.mention for user in selected_winners)
    await ctx.send(f"Congratulations {winners_mention}! You won {prize}!")

@giveaway.error
async def giveaway_error(ctx, error):
    if isinstance(error, commands.MissingPermissions):
        embed = discord.Embed(title="SOMETHING WENT WRONG", description="You currently do not have the correct permissions to use this command. You need the **Manage Guild** permission to use this command", color=(16711839))
        await ctx.send(embed=embed)
    if isinstance(error, commands.MissingRequiredArgument):
        embed = discord.Embed(title="SOMETHING WENT WRONG", description="You did not provide enough arguments for this command.\nfc!giveaway <time> <winners> <prize>", color=(16711839))
        await ctx.send(embed=embed)
    else:
        raise error

@client.command()
@commands.has_permissions(manage_messages=True)
async def poll(ctx, question, option1: str, option2: str):
    poll_embed = discord.Embed(title=question, color=167935, description=f"1️⃣ {option1}\n2️⃣ {option2}")
    react_message = await ctx.send(embed=poll_embed)
    await react_message.add_reaction('1️⃣')
    await react_message.add_reaction('2️⃣')

@client.command()
@commands.has_permissions(manage_messages=True)
async def threepoll(ctx, question, option1: str, option2: str, option3: str):
    poll_embed = discord.Embed(title=question, color=167935, description=f"1️⃣ {option1}\n2️⃣ {option2}\n3️⃣ {option3}")
    react_message = await ctx.send(embed=poll_embed)
    await react_message.add_reaction('1️⃣')
    await react_message.add_reaction('2️⃣')
    await react_message.add_reaction('3️⃣')

@poll.error
async def poll_error(ctx, error):
    if isinstance(error, commands.MissingPermissions):
        embed = discord.Embed(title="SOMETHING WENT WRONG", description="You currently do not have the correct permissions to use this command. You need the **Manage Guild** permission to use this command", color=(16711839))
        await ctx.send(embed=embed)
    if isinstance(error, commands.MissingRequiredArgument):
        embed = discord.Embed(title="SOMETHING WENT WRONG", description="You did not provide enough arguments for this command.\nfc!poll <question> <answer1> <answer2>", color=(16711839))
        await ctx.send(embed=embed)
    else:
        raise error

@threepoll.error
async def threepoll_error(ctx, error):
    if isinstance(error, commands.MissingPermissions):
        embed = discord.Embed(title="SOMETHING WENT WRONG", description="You currently do not have the correct permissions to use this command. You need the **Manage Guild** permission to use this command", color=(16711839))
        await ctx.send(embed=embed)
    if isinstance(error, commands.MissingRequiredArgument):
        embed = discord.Embed(title="SOMETHING WENT WRONG", description="You did not provide enough arguments for this command.\nfc!poll <question> <answer1> <answer2> <answer3>", color=(16711839))
        await ctx.send(embed=embed)
    else:
        raise error

@client.command()
async def numberguessing(ctx):
    def check(m):
        return m.author == ctx.author and m.channel == ctx.message.channel
    number = random.randint(1, 100)
    await ctx.send("I have a number in mind between 1 and 100. You have 15 chances to guess the right number")

    for i in range(0, 10):
        guess = await client.wait_for('message', check=check)
        if guess.content == str(number):
            await ctx.send("You won. If you want, you can start another game by running fc!numberguessing")
            return
        elif guess.content < str(number):
            await ctx.send('Higher!')
        elif guess.content > str(number):
            await ctx.send('Lower!')
        else:
            return     
    else:
        await ctx.send("You lost. However, you can try again by running fc!numberguessing again.")

@client.command()
async def rps(ctx):
    rpsGame = ['rock', 'paper', 'scissors']
    await ctx.send("Rock, paper, or scissors?")

    def check(msg):
        return msg.author == ctx.author and msg.channel == ctx.channel and msg.content.lower() in rpsGame

    user_choice = (await client.wait_for('message', check=check)).content

    comp_choice = random.choice(rpsGame)
    if user_choice == 'rock':
        if comp_choice == 'rock':
            await ctx.send(f"STATUS: **tied**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'paper':
            await ctx.send(f"STATUS: **Lost**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'scissors':
            await ctx.send(f"STATUS: **Won**\nYour choice: {user_choice} | My choice: {comp_choice}")

    elif user_choice == 'paper':
        if comp_choice == 'rock':
            await ctx.send(f"STATUS:**Won**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'paper':
            await ctx.send(f"STATUS: **tied**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'scissors':
            await ctx.send(f"STATUS: **Lost**\nYour choice: {user_choice} | My choice: {comp_choice}")

    elif user_choice == 'scissors':
        if comp_choice == 'rock':
            await ctx.send(f"STATUS: **Lost**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'paper':
            await ctx.send(f"STATUS: **Won**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'scissors':
            await ctx.send(f"STATUS: **tied**\nYour choice: {user_choice} | My choice: {comp_choice}")

@client.command()
async def say(ctx, *, question: commands.clean_content):
    await ctx.send(f'{question}')
    await ctx.message.delete()

@say.error
async def say_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        embed = discord.Embed(title="SOMETHING WENT WRONG", description="HUH???????", color=(16711839))
        embed.set_footer(text="You did not provide a text for me say. Example: fc!say Hi")
        await ctx.send(embed=embed)
    else:
        raise error

@client.command()
async def esay(ctx, *, question):
    embed = discord.Embed(description=f'{question}', color=(16745984))
    await ctx.send(embed=embed)
    await ctx.message.delete()

@esay.error
async def esay_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        embed = discord.Embed(title="SOMETHING WENT WRONG", description="HUH???????", color=(16711839))
        embed.set_footer(text="You did not provide a text for me say. Example: fc!esay Hi")
        await ctx.send(embed=embed)
    else:
        raise error

@client.command(aliases=["si"])
async def serverinfo(ctx):
    format = "%a, %b %d %Y | %H:%M:%S %ZGMT"   
    embed = discord.Embed(title="SERVER INFORMATION", description=f"Here is some Information about {ctx.guild.name}", color=(16745984))
    embed.add_field(name="Basic Information", value=f"Guild Name: {ctx.guild.name}\nMember Count: {ctx.guild.member_count}\nGuild ID: {ctx.guild.id}\nServer Owner: {ctx.guild.owner}\nVerification Level: {ctx.guild.verification_level}\nServer Creation Date: {ctx.guild.created_at.strftime(format)}", inline=False)
    text_channels = len(ctx.guild.text_channels)
    voice_channels = len(ctx.guild.voice_channels)
    categories = len(ctx.guild.categories)
    channels = text_channels + voice_channels
    embed.add_field(name="Channel Count", value=f"Categories; **{categories}**\nText Channels; **{text_channels}**\nVoice Channels; **{voice_channels}**\nTotal Channels: **{channels}**", inline=False)
    embed.set_thumbnail(url=ctx.guild.icon)
    await ctx.send(embed=embed)

@client.command(aliases=["ui"])
async def userinfo(ctx, *, member: discord.Member = None):
    if member is None:
        member = ctx.author
    date_format = "%a, %b %d %Y %I:%M %p"
    embed = discord.Embed(title=f"USER INFORMATION/STATS for {member.mention}", color=(16745984))
    embed.add_field(name="BASIC INFO", value=f"Display Name: {member.display_name}\nUsername: {member.name}\nUser ID: {member.id}\nRegistered at: {member.created_at.strftime(date_format)}", inline=False)
    embed.add_field(name="SERVER BASED INFO", value=f"Joined Server at: {member.joined_at.strftime(date_format)}", inline=False)
    if len(member.roles) > 1:
        role_string = ' '.join([r.mention for r in member.roles][1:])
        embed.add_field(name="ROLES [{}]".format(len(member.roles)-1), value=role_string, inline=False)
    perm_string = ', '.join([str(p[0]).replace("_", " ").title() for p in member.guild_permissions if p[1]])
    embed.set_thumbnail(url=member.avatar)
    await ctx.send(embed=embed)

@client.command()
@commands.cooldown(1, 30, commands.BucketType.user)
async def robloxuserinfo(ctx, user_id):
    jsonreq=requests.get(f"https://users.roblox.com/v1/users/{user_id}").json()
    request=requests.get(f"https://friends.roblox.com/v1/users/{user_id}/followers/count").json()
    thumbnail_json = requests.get(f"https://thumbnails.roblox.com/v1/users/avatar-headshot?userIds={user_id}&size=100x100&format=Png&isCircular=false")
    thumbnail = json.loads(thumbnail_json.text)
    thumbnail_url = thumbnail['data'][0]['imageUrl']
   
    embed = discord.Embed(title=f"{jsonreq['displayName']}", url=f"https://www.roblox.com/users/{user_id}/profile", color=0x00b3ff)
    embed.add_field(name="Basic Information", value=f"Username: {jsonreq['name']}\nDisplay Name: {jsonreq['displayName']}\nRoblox ID: {jsonreq['id']}\nAccount Created: {jsonreq['created']}", inline=False)
    embed.add_field(name="About", value=jsonreq['description'], inline=False)
    embed.add_field(name="Account Ban", value=jsonreq['isBanned'], inline=False)
    embed.add_field(name="Following", value=f"Followers: {request['count']}", inline=False)
    embed.set_thumbnail(url=f"{thumbnail_url}")
    await ctx.send(embed=embed)

@robloxuserinfo.error
async def robloxuserinfo_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        embed = discord.Embed(title="SOMETHING WENT WRONG", description="You did not provide enough arguments for this command.\nfc!robloxuserinfo <robloxid>", color=(16711839))
        await ctx.send(embed=embed)
    if isinstance(error, commands.CommandOnCooldown):
        embed = discord.Embed(title="SOMETHING WENT WRONG", description="To avoid Roblox API abuse, this command has a cooldown of 30 seconds per user. Please try again later.", color=(16711839))
        await ctx.send(embed=embed)
    else:
        raise error





botstaff = [757827467657478245]

@client.command()
async def watchstatus(ctx, *, question):
    if ctx.author.id in botstaff:
        await client.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=f"{question}"))
        await ctx.send("My Status is Successfully Changed")
    else:
        await ctx.send("This command can only be used by the bot admins")
        return

@client.command()
async def listenstatus(ctx, *, question):
    if ctx.author.id in botstaff:
        await client.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name=f"{question}"))
        await ctx.send("My Status is Successfully Changed")
    else:
        await ctx.send("This command can only be used by the bot admins")
        return

@client.command()
async def playstatus(ctx, *, question):
    if ctx.author.id in botstaff:
        await client.change_presence(activity=discord.Game(name=f"{question}"))
        await ctx.send("My Status is Successfully Changed")
    else:
        await ctx.send("This command can only be used by the bot admins")
        return





TOKEN = os.getenv("DISCORDBOTTOKEN")
client.run(TOKEN)
