import voltage
from voltage.ext import commands
import random
import asyncio
import aiohttp
import requests
import json
import os
from dotenv import load_dotenv

load_dotenv()

client = commands.CommandsClient(os.getenv("PREFIX"))

@client.listen("ready")
async def ready():
    print("Funtime Chica Revolt Bot is Online and Ready")

@client.command(description="View Information about the bot + Invite Links")
async def botinfo(ctx):
    embed = voltage.SendableEmbed(title="Funtime Chica Information", description=f"Servers Im in: {len(client.servers)}\nInvite Me: https://app.revolt.chat/bot/01GX9EMCK7HTGXYREB2N3JQHD4\nSupport Server: https://rvlt.gg/sRTVE2sV\nSource Code: https://gitlab.com/idiotcreaturehaterstudios/funtimechica", color='#FF8600')
    await ctx.send(embed=embed)

@client.command(description="Setup a giveaway in your server. fc!giveaway <time> <winners> <prize>")
async def giveaway(ctx, time_unit: str, winners: int, *, prize: str):
    if not ctx.author.permissions.manage_server:
        await ctx.send("You currently do not have the correct permissions to use this command. You need the **Manage Server** permission to use this command")
        return
    embed = voltage.SendableEmbed(title="Giveaway!", description=f"{prize}\nReact with 🎉 to enter!\nEnds {time_unit} from now", color="#FF8600")
    msg = await ctx.send(embed=embed)
    await msg.react("🎉")
    duration_seconds = convert(time_unit)
    await asyncio.sleep(duration_seconds)
    new_msg = await ctx.channel.fetch_message(msg.id)
    
    members = []
    async for member in new_msg.reactions["🎉"].members():
        if member != client.user:
          members.append(member)

    winners_mention = random.choice(new_msg.reactions["🎉"])
    await ctx.send(f"Congratulations {winners_mention}! You won {prize}!")

@client.command(description="Setup a poll in your server. fc!poll <question> <answer1> <answer2> [answer3] [answer4] [answer5]")
async def poll(ctx, question, option1: str, option2: str, option3: str = None, option4: str = None, option5: str = None):
    if not ctx.author.permissions.manage_server:
        await ctx.send("You currently do not have the correct permissions to use this command. You need the **Manage Server** permission to use this command")
        return
    if option5:
        poll_embed = voltage.SendableEmbed(title=question, color="#FF8600", description=f"1️⃣ {option1}\n2️⃣ {option2}\n3️⃣ {option3}\n4️⃣ {option4}\n5️⃣ {option5}")
        new_msg = await ctx.send(embed=poll_embed)
        await new_msg.react('1️⃣')
        await new_msg.react('2️⃣')
        await new_msg.react('3️⃣')
        await new_msg.react('4️⃣')
        await new_msg.react('5️⃣')
        return
    if option4:
        poll_embed = voltage.SendableEmbed(title=question, color="#FF8600", description=f"1️⃣ {option1}\n2️⃣ {option2}\n3️⃣ {option3}\n4️⃣ {option4}")
        new_msg = await ctx.send(embed=poll_embed)
        await new_msg.react('1️⃣')
        await new_msg.react('2️⃣')
        await new_msg.react('3️⃣')
        await new_msg.react('4️⃣')
        return
    if option3:
        poll_embed = voltage.SendableEmbed(title=question, color="#FF8600", description=f"1️⃣ {option1}\n2️⃣ {option2}\n3️⃣ {option3}")
        new_msg = await ctx.send(embed=poll_embed)
        await new_msg.react('1️⃣')
        await new_msg.react('2️⃣')
        await new_msg.react('3️⃣')
        return
    if option2:
        poll_embed = voltage.SendableEmbed(title=question, color="#FF8600", description=f"1️⃣ {option1}\n2️⃣ {option2}")
        new_msg = await ctx.send(embed=poll_embed)
        await new_msg.react('1️⃣')
        await new_msg.react('2️⃣')
        return

@client.command(description="Make me say things")
async def say(ctx, *, question):
    await ctx.send(f'{question}')
    await ctx.message.delete()

@client.command(description="Make me say things in a embed")
async def esay(ctx, *, question):
    embed = voltage.SendableEmbed(description=f'{question}', color="#FF009F")
    await ctx.send(embed=embed)
    await ctx.message.delete()

@client.command(description="Play the Number Guessing game with the bot")
async def numberguessing(ctx):
    def check(m):
        return m.author == ctx.author and m.channel == ctx.message.channel
    number = random.randint(1, 100)
    await ctx.send("I have a number in mind between 1 and 100. You have 15 chances to guess the right number")

    for i in range(0, 15):
        guess = await client.wait_for('message', check=check)
        if guess.content == str(number):
            await ctx.send("You won. If you want, you can start another game by running fc!numberguessing")
            return
        elif guess.content < str(number):
            await ctx.send('Higher!')
        elif guess.content > str(number):
            await ctx.send('Lower!')
        else:
            return     
    else:
        await ctx.send("You lost. However, you can try again by running fc!numberguessing again.")

@client.command(description="Play Rock Paper Scissors with the bot")
async def rps(ctx):
    rpsGame = ['rock', 'paper', 'scissors']
    await ctx.send("Rock, paper, or scissors?")

    def check(msg):
        return msg.author == ctx.author and msg.channel == ctx.channel and msg.content.lower() in rpsGame

    user_choice = (await client.wait_for('message', check=check)).content

    comp_choice = random.choice(rpsGame)
    if user_choice == 'rock':
        if comp_choice == 'rock':
            await ctx.send(f"STATUS: **tied**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'paper':
            await ctx.send(f"STATUS: **Lost**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'scissors':
            await ctx.send(f"STATUS: **Won**\nYour choice: {user_choice} | My choice: {comp_choice}")

    elif user_choice == 'paper':
        if comp_choice == 'rock':
            await ctx.send(f"STATUS:**Won**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'paper':
            await ctx.send(f"STATUS: **tied**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'scissors':
            await ctx.send(f"STATUS: **Lost**\nYour choice: {user_choice} | My choice: {comp_choice}")

    elif user_choice == 'scissors':
        if comp_choice == 'rock':
            await ctx.send(f"STATUS: **Lost**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'paper':
            await ctx.send(f"STATUS: **Won**\nYour choice: {user_choice} | My choice: {comp_choice}")
        elif comp_choice == 'scissors':
            await ctx.send(f"STATUS: **tied**\nYour choice: {user_choice} | My choice: {comp_choice}")

@client.command(description="Get information about your server")
async def serverinfo(ctx):
    embed = voltage.SendableEmbed(title="Server Information", description=f"Guild Name: {ctx.server.name}\nGuild ID: {ctx.server.id}\nServer Owner: {ctx.server.owner.name} (`{ctx.server.owner.id}`)\nServer Creation Date: {ctx.server.created_at}", color='#FF8600')
    await ctx.send(embed=embed)

@client.command(description="Get information about a user")
async def userinfo(ctx, member: voltage.Member = None):
    if member is None:
        member = ctx.author
    embed = voltage.SendableEmbed(title="User Information", description=f"User Name: {member.name}\nUser ID: {member.id}", color='#FF8600')
    await ctx.send(embed=embed)

@client.command(description="Get information on a Roblox user. fc!robloxuserinfo <robloxid>")
async def robloxuserinfo(ctx, user_id):
    jsonreq=requests.get(f"https://users.roblox.com/v1/users/{user_id}").json()
    request=requests.get(f"https://friends.roblox.com/v1/users/{user_id}/followers/count").json()
    thumbnail_json = requests.get(f"https://thumbnails.roblox.com/v1/users/avatar-headshot?userIds={user_id}&size=100x100&format=Png&isCircular=false")
    thumbnail = json.loads(thumbnail_json.text)
    thumbnail_url = thumbnail['data'][0]['imageUrl']
    embed = voltage.SendableEmbed(title=f"{jsonreq['displayName']}", description=f"UserName: {jsonreq['name']}\nDisplay Name: {jsonreq['displayName']}\nRoblox ID: {jsonreq['id']}\nAccount Created: {jsonreq['created']}\nAccount Ban: {jsonreq['isBanned']}\nFollowers: {request['count']}\nProfile Link: https://www.roblox.com/users/{user_id}/profile\nAbout: {jsonreq['description']}", media=thumbnail_url, color="#FF009F")
    await ctx.send(embed=embed)

@client.error("message")
async def on_message_error(error: Exception, message):
    if isinstance(error, NotEnoughArgs):
        await message.reply("Something went wrong. You did not provide enough arguments for this command. Run **help <commandname>** to see what arguments are needed")

botstaff = ["01GQ6QGE69CCXZ22GKXGBMKDRB"]

@client.command()
async def status(ctx, *, question):
    if ctx.author.id in botstaff:
        await client.set_status(text=f"{question}")
        await ctx.send("My Status is Successfully Changed")
    else:
        await ctx.send("This command can only be used by the Bot Owner/Admins")
        return





TOKEN = os.getenv("REVOLTBOTTOKEN")
client.run(TOKEN)
