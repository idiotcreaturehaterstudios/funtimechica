# Funtime Chica Source code

Here is the source code for the Funtime Chica Discord and Revolt bot. Guilded Bot is also included but no longer supported.

Features (For Discord and Revolt bot):
- Giveaways
- Polls
- Minigames
- Stats

# Links

THIS SOURCE CODE IS DISCONTINUED. [VIEW THE NEW SOURCE CODE](https://gitlab.com/eagletribeeris/funtimechica) BY THE NEW OWNER which is now license under the Creative Commons Zero v1.0 Universal license (which is pretty similar to the Unlicense license except they don't provide Trademark use or Patent use)). If you wish to use this version, you are on your own.

The new Discord bot's ID is: 1062643287409819698 (Developed by 720847987756105809 which is who I allowed to continue the project)

The new Revolt bot's ID is: 01GX2JTGGNNFQFCEJRACYWT8AJ (Developed by 01GQY9BCKAVNB1WN2AF6QYDZBG which is who I allowed to continue the project)

# Bot Setup Guide

Before starting, make sure you install all the packages in the requirements.txt file.

DISCORD BOT SETUP GUIDE
1. Fork/Download this source code
2. Go to https://discord.com/developers/applications and create a discord bot application, create a bot, and copy its discord bot token if you have not already done so.
- Make sure to enable the Message Content intent.
3. Open .env.example and fill in everything it asks for.
- Copy and Paste your bot token in DISCORDBOTTOKEN= in .env.example
- Put in the prefix you want to use in PREFIX=
4. Setting up Bot Admins
-- Open FuntimeChicaDiscord.py and head to botstaff = [757827467657478245]
-- Replace the ID with the ids of the people who want to be your Bot admins. Use commas and spaces to seperate. See example below. NOTE: 757827467657478245 is a user id of the orignal creator
-- Example: botstaff = [757827467657478245, 924910621085040690, 302050872383242240] == In the example, I just put 3 users to be a Bot Admin
5. Change .env.example to .env and run the bot: python FuntimeChicaDiscord.py

REVOLT BOT SETUP GUIDE
1. Fork/Download this source code.
2. Go to the "My Bots" section of your settings and create a bot.
-- For app.revolt.chat (Nightly) users, this is https://app.revolt.chat/settings/bots
3. Open .env.example and fill in everything
- Copy and Paste your bot token in REVOLTBOTTOKEN= in .env.example
- Put in the prefix you want to use in PREFIX=
4. Install Voltage. (You have to install it through git. Installing this is included in the requirements.txt file)
5. Change .env.example to .env and run the bot: python FuntimeChicaRevolt.py

# Disclaimers

THE GUILDED BOT IS NO LONGER SUPPORTED! IF YOU WISH TO USE FuntimeChicaGuilded.py, YOU ARE ON YOUR OWN!

This source code is here for people to help improve the Funtime Chica bots. If you wish to use this source code for your own work, you are on your own as we will NOT help you setup the bot. If you wish to use this, we expect you to have some knowledge with discord.py, voltage, and Python.